
#include <stdio.h>
#include <string.h>
struct employee
{
 char name[50];
 int id;
 float salary;
 char date[8];
};
int main()
{
 struct employee e[50];
 int i,n;
 printf("Enter the number of employees\n");
 scanf("%d",&n);
 for(i=0;i<n;i++)
 { 
  printf("Enter the Employee %d details\n",i+1);
  printf("Enter the employee ID\n");
  scanf("%d",&e[i].id);
  printf("Enter the employee name\n");
  scanf("%s",e[i].name);
  printf("Enter the date of joining of the employee in the format(dd/mm/yy)\n");
  scanf("%s",e[i].date);
  printf("Enter the salary of employee\n");
  scanf("%f",&e[i].salary);
 }
 for(i=0;i<n;i++)
 {
  printf("The Employee %d details are\n",i+1);
  printf("The employee ID-\n");
  printf("%d\n",e[i].id);
  printf("The employee name is\n");
  printf("%s",e[i].name);
  printf("\n");
  printf("The date of joining of the employee in the format(dd/mm/yy) is\n");
  printf("%s",e[i].date);
  printf("\n");
  printf("The salary of employee is\n");
  printf("%f\n",e[i].salary);
 }
 return 0;
}
  