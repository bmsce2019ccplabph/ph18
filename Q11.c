#include <stdio.h>
void main()
{
    int sub1,sub2,sub3,sub4,total;
    float avg;
    char grade;
    printf("Enter the marks(MAX=100) of\n");
    printf("Physics\n");
    scanf("%d",&sub1);
    printf("Maths\n");
    scanf("%d",&sub2);
    printf("Chemistry\n");
    scanf("%d",&sub3);
    printf("Computer\n");
    scanf("%d",&sub4);
    total=sub1+sub2+sub3+sub4;
    avg=(float)total/4.0;
    if(avg>=90)
    grade='S';
    else if(avg<90&&avg>=80)
    grade='A';
    else if(avg<80&&avg>=70)
    grade='B';
    else if(avg<70&&avg>=60)
    grade='C';
    else if(avg<60&&avg>=50)
    grade='D';
    else if(avg<50&&avg>=40)
    grade='E';
    else
    grade='F';
    printf("The marks of\n Physics=%d\n Maths=%d\n Chemistry=%d\n Computer=%d\n",sub1,sub2,sub3,sub4);
    printf(" Total marks=%d\n Aggregated marks=%.2f\n Grades=%c\n",total,avg,grade);
   }
