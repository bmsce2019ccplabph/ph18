#include <stdio.h>
void input(int *,int *);
void add(int ,int ,int *);
void output(int *);
void input(int *a,int *b)
{
	printf("Enter the numbers to be added\n");
	scanf("%d %d",a,b);
}
void add(int a,int b,int *sum)
{ 
   *sum=a+b;
}
void output(int *sum)
{ 
  printf("The sum of two numbers is %d \n",*sum);
}
int main()
{ 
	int a,b,sum;
	input(&a,&b);
	add(a,b,&sum);
	output(&sum);
}
