
#include<stdio.h>
void input(int *a,int n)
{
	int i;
	printf("Enter the numbers of the array\n");
	for(i=0;i<n;i++)
	 scanf("%d",&a[i]);
}
int small(int *a,int n)
{
    int i,min;
    min=0;
	for(i=0;i<n;i++)    
    {
      if(a[i]<a[min])
        min=i;
    }
    printf("The minimum in the array is %d\n",a[min]); 
    return min;
}
int largest(int *a,int n)
{
    int i,max;
    max=0;
	for(i=1;i<n;i++)    
    {
      if(a[i]>a[max])
        max=i;
    }
   printf("The maximum in the array is %d\n",a[max]); 
    return max;
}
void compute(int *a,int max,int min)
{
	int temp;
    temp=a[max];
    a[max]=a[min];
    a[min]=temp;
}
void output(int *a,int n)
{   
	int i;
    for(i=0;i<n;i++)
	 printf("%d\t",a[i]);
    printf("\n");
}
int main()
{
	int n,a[1000],min,max;
	printf("Enter the number of elements in array\n");
	scanf("%d",&n);
	input(a,n);
    min=small(a,n);
    max=largest(a,n);
    printf("The array before interchangeing largest and smallest\n");
    output(a,n);
	compute(a,max,min);
    printf("The array after interchangeing largest and smallest\n");   
	output(a,n);
}
