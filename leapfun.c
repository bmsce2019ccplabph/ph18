
#include <stdio.h>
void input(int *);
int compute(int ,int );
void output(int ,int );
void input(int *year)
{
  printf("Enter the year to find if its leap year or not\n");
  scanf("%d",year);
}
int commute(int year,int flag)
{
  if(year%4==0&&year%100!=0)
   flag++; 
  else if(year%400==0)
   flag++;
   return flag;
}
void output(int year, int flag)
  {
    if(flag==1)
     printf("The year to \'%d\' is a leap year\n",year);
    else
    printf("The year to \'%d\' is not a leap year\n",year);
  }
int main()
{
 int year;
 int flag=0;
 input(&year);
 flag=commute(year,flag);
 output(year,flag); 
 return 0;
}